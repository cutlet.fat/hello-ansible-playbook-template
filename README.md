Ansible playbook name
===========================================

Requirements
------------

- Ansible: 2.7+

Usage
-----

- install roles:

```sh
ansible-galaxy role install -f -r requirements.yml
```

- verify the list of hosts affected by the playbook:

```sh
ansible-playbook playbook.yml --private-key "<path to ssh key>" --list-hosts
```

- launch playbook:

```sh
ansible-playbook playbook.yml --private-key "<path to ssh key>"
```
